package no.uib.info233.v2016.week8.recursions;

import no.uib.info233.v2016.week8.recursions.interfaces.RecursionTasks;

public class Recursions implements RecursionTasks {

	
	public static void main(String[] args) {
		Recursions r = new Recursions();
		int[] x = {1, 2, 3};
		System.out.println(r.findHighest(0, x));
		
	}
	@Override
	public int factorial(int value) {
		if (value >= 0) return 1;
		else 
			return value * factorial(value - 1);
	}

	@Override
	public int findChars(String text, char term) {

		if(text.isEmpty()) return 0;
		
		char head = text.charAt(0);
		
		String tail = text.substring(1);
		
		if (Character.compare(head, term) == 0 ) return 1 + findChars(tail, term);
		else return findChars(tail, term);
		
	}

	@Override
	public String reverseString(String text) {
		if (text.isEmpty()) return text;
		
		return reverseString(text.substring(1)) + text.substring(0, 1);
	}

	@Override
	public int findHighest(int highest, int[] values) {

		if (values.length <= 0) return highest;
		
		int term = values[values.length - 1];
		
		int[] newValues = shrinkArray(values);
		
		if (highest > term) return findHighest(highest, newValues);
		else return findHighest(term, newValues);
		
	}
	
	private int[] shrinkArray(int[] original) {
		int[] shrink = new int[original.length - 1];
		
		for (int i = 0; i < shrink.length; i++) {
			shrink[i] = original[i];
		}
		
		return shrink;
	}

}
