package no.uib.info233.v2016.week8.recursions.interfaces;

public interface RecursionTasks {
	
	
	/**
	 * This method is supposed to recursively find the factorial value of any integer it receives as a parameter.
	 * 
	 * @param an integer which it will find the factorial value of.
	 * @return an integer which is the factorial of the parameter.  
	 */
	
	public int factorial(int value);
	
	
	/**
	 * This method is supposed to find all specified characters in a given string using recursion. It will return an integer which represents the amount of specified characters occuring in the string parameter.
	 * 
	 * @param String a string to search through. 
	 * @param char a character which the method will search for in the first parameter.
	 * @return int representation of the amount of times a char appears in the string.
	 * 
	 */

	public int findChars(String text, char term);
	
	/**
	 * This method is supposed to reverse a string using recursion. 
	 * 
	 * @param String the string to reverse.
	 * @return String the reversed string.
	 */
	
	public String reverseString(String text);
	
	/**
	 * This method is supposed to find the highest value integer in an array using recursion.
	 * 
	 * @param int the currently highest integer value.
	 * @param int[] an array of random integers
	 * @return int the highest value in the array. 
	 */
	
	public int findHighest(int highest, int[] values);
	
}
