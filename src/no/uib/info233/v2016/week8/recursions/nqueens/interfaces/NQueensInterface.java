package no.uib.info233.v2016.week8.recursions.nqueens.interfaces;

/**
 * The class implementing this supposed to solve the N Queens problem using recursion and backtracking. 
 * Simply put, the N Queens problem involves putting an amount of queens (from the Chess game) on a table that is the same width and height as the amount of queens. 
 * Assume that each queen is hostile to each other, and that they are supposed to be placed in such a way that they cannot attack another queen with their first move.
 * 
 * Assume that the queens position in the array is the same as the Y axis position, and that the integer stored in the position is the X-axis location.
 * 
 * 
 * @author skl024
 *
 */

public interface NQueensInterface {

	/**
	 * This method must check if the queens are placed consistently. It should only return true if the queens are placed in non-threatened locations. 
	 * 
	 * 
	 * @param int[] q This array is supposed to represent the queens placement on the board. It should be the same size as N. 
	 * @param int n the queen's y-axis position
	 * @return boolean true if the queen positions are not threatened. 
	 */
	boolean isConsistent(int[] q, int n);
	
	/**
	 * This method should print out the game board in ASCII symbols. 
	 * 
	 * For example:
	 * 
	 * Queens: 4
	 *  * Q * *
	 *  * * * Q
	 *  Q * * *
	 *  * * Q *
	 * 
	 * @param int[] q the array of queens. 
	 */
	void printQueens(int[] q);
	
	/**
	 * This method should start the process of solving the problem. 
	 * 
	 * @param N - The amount of queens, as well as board width and length.
	 */
	void enumerate(int N);
	
	/**
	 * This method should be the primary recursive method. It has to:
	 * 1 check if queens are placed properly
	 * 2 attempt to add more queens
	 * 3 stop when it has placed the n-th queen
	 * 4 print the game board
	 * 
	 * @param int[] q the array of queens.
	 * @param int n the current queen the method is supposed to place.
	 */
	void enumerate(int[] q, int n);
	
	
}
