package no.uib.info233.v2016.week8.recursions.ancestors.interfaces;

import no.uib.info233.v2016.week8.recursions.ancestors.Parent;

/**
 * The class implementing this interface is supposed to figure out if two given people are related using recursion. 
 * 
 * Try to solve this problem with as few recursive methods as possible. 
 * 
 * @author skl024
 *
 */

public interface AncestorsInterface {
	
	/**
	 * This method should find any parents belonging to the name entered as the second parameter.
	 * 
	 * @param Parent[] parents the list of parents, as found in the AncestorProblem class.
	 * @param String child the name of the child. 
	 * @return String[] a string array of names that match as parents to the child name. 
	 */
	String[] findParents(Parent[] parents, String child);
	
	
	/**
	 * This method should find any ancestors related to the name entered as the second parameter. 
	 * That is, it should find the name's parents, as well as parents of parents until the list is exhausted.
	 * 
	 * @param Parent[] parents the list of parents, as found in the AncestorProblem class.
	 * @param String child the name of the child. 
	 * @return String[] a string array of names that match as ancestors to the child name.
	 */
	String[] findAncestors(Parent[] parents, String child);
	
	/**
	 * This is intended to be a support method. Should be used to test if a name supports the relation the method name suggests.
	 * 
	 * @param Parent[] parents the list of parents, as found in the AncestorProblem class. 
	 * @param String parent - the name of the parent.
	 * @param String child - the name of the child.
	 * @return True if the parent name actually is a parent of someone with the child's name. 
	 */
	boolean isParentOf(Parent[] parents, String parent, String child);
	
	/**
	 * This is intended to be a support method. Should be used to check if a given name is also the name of a parent.
	 * 
	 * @param Parent[] parents the list of parents, as found in the AncestorProblem class.
	 * @param String name the name to be tested. 
	 * @return True if the name is listed as a parent of someone. 
	 */
	boolean hasChild(Parent[] parents, String name);
	
	/**
	 * This is intended to be a support method. Should be used to check if a given name is also the name of a child.
	 * 
	 * @param Parent[] parents the list of parents, as found in the AncestorProblem class.
	 * @param String name the name to be tested. 
	 * @return True if the name is listed as a child of someone. 
	 */
	boolean hasParent(Parent[] parents, String name);
	

}
