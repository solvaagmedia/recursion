package no.uib.info233.v2016.week8.recursions.ancestors;

public class Parent {
	
	private String parent;
	private String child;
	
	
	public Parent(String parent, String child) {
		this.parent = parent;
		this.child = child;
	}
	
	public String getParent() {
		return parent;
	}
	
	public String getChild() {
		return child;
	}
	
	
	
}
